#include <SoftwareSerial.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

String str;

int counter=0;

int const PULSE_SENSOR_PIN = 0;

int Signal;
int Threshold = 400; 
LiquidCrystal_I2C lcd(0x3F,16,2);

void setup() {
  pinMode(LED_BUILTIN,OUTPUT);
  Serial.begin(115200);
  lcd.begin();
  lcd.clear();         
  lcd.backlight();
  lcd.setCursor(0,0);
  lcd.print("Your pulse is:");
}

void loop() {
  Signal = analogRead(PULSE_SENSOR_PIN);
  Serial.println("Signal");
  Serial.println(Signal - 850); 

  if((Signal - 850) < Threshold && Signal > 50){         
    lcd.setCursor(0,1);
    lcd.print(Signal - 850);
    delay(2000);
    lcd.print(" ");
  }else{
    lcd.setCursor(5,1);
    lcd.print("...");
  }
}